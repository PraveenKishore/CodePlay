# Import datasets, classifiers and performance metrics
from sklearn import datasets, svm, metrics
import numpy as np
import cv2

import warnings
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)
warnings.filterwarnings("ignore", category=DeprecationWarning)

train = [2]*10
labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'] # np.arange(10)

for i in range(0, 10):
    train[i] = np.asarray(cv2.imread("Images/{}.jpg".format(i), cv2.COLOR_BGR2GRAY)).reshape(-1)

classifier = svm.SVC(gamma=0.001)
classifier.fit(train, labels)

for i in range(9, 0, -1):
    inputImagePath = "Images/{}.jpg".format(i)
    inputImage = np.asarray(cv2.imread(inputImagePath, cv2.COLOR_BGR2GRAY)).reshape(-1)
    print("Image: {}, Prediction: {}".format(inputImagePath, classifier.predict(inputImage)[0]))

from random import randint
i = randint(0, 9)
inputImagePath = "Images/{}.jpg".format(i)
inputImage = np.asarray(cv2.imread(inputImagePath, cv2.COLOR_BGR2GRAY)).reshape(-1)
print("Random Input image: {}, Prediction: {}".format(inputImagePath, classifier.predict(inputImage)[0]))
